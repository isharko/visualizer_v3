//
//  Controller.cpp
//  Visualizer_v3
//
//  Created by Ivan Sharko on 2013-05-28.
//
//

#include "Controller.h"

Controller::Controller(){

}

Controller::Controller(int &MESH_SIZE){
    mMeshSize = MESH_SIZE;
}

void Controller::update( float dt ){
    updateFFT();
}

void Controller::updateFFT(){
    mFFTController.update();
    mFftTexture = mFFTController.getFFTTexture(mMeshSize);
}

void Controller::drawFFT(){
    mFFTController.drawFFT();
}

void Controller::drawFFTTexture(){
    
}


