#include "FFTController.h"

using namespace ci;
using namespace std;

FFTController::FFTController( )
{
    //initialize the audio Input, using the default input device
	mInput = audio::Input();
	
	//tell the input to start capturing audio
	mInput.start();
    
    FFTSumC         = 0;
    FFTSumP         = 0;
    FFTDifference   = 0;
    FREQBUCKETS     = 200;
    BASEBANDS       = 3;
    
}

void FFTController::update(){
    mPcmBuffer = mInput.getPcmBuffer();
	if( ! mPcmBuffer ) {
		return;
	}
    #if defined( CINDER_MAC )
        uint16_t bandCount = FREQBUCKETS;
        //presently FFT only works on OS X, not iOS
        mFftDataRef = audio::calculateFft( mPcmBuffer->getChannelData( audio::CHANNEL_FRONT_LEFT ), bandCount );
    #endif
	
	if( ! mFftDataRef ) {
		return;
	}
    
    fftBuffer = mFftDataRef.get();
//    for( list<Particle>::iterator particleIt = particles.begin(); particleIt != particles.end(); ++particleIt ){
//        
//        float barY = fftBuffer[particleIt->frequencyBucket] / bandCount;
//        
//        particleIt -> mFreq = barY;
//    }

    
    //Get FFT variables to track changes in music
    FFTSumC = getFFTSum(0, FREQBUCKETS);
    FFTDifference = FFTSumC - FFTSumP;
    
    FFTBaseSum = getFFTSum(0, BASEBANDS); // first 3 Frequency bands
    FFTBaseMax = getFFTMax(0, BASEBANDS);
    FFTBaseDifference = FFTBaseSum - FFTBaseP;
    FFTBaseAverage = FFTBaseSum / 5.0f;

    // If FFT across the board is low, then it's a "quiet"
    if (fabs(FFTSumC) < 170  && quiet == true){
        std::cout << "quiet!!!!!!!!!!!!   " << FFTSumC << std::endl;
        glColor3f( 255.0f, 0.0f, 0.0f );
        gl::drawSolidCircle( Vec2f( 15.0f, 50.0f ), 10.0f );
        quiet = true;
    } else {
        quiet = false;
    }
    
    //Add current FFT to FFT history
    FFTBaseAverages.push_back(FFTBaseAverage);
    if (k < 20){
        k++;
    } else {
        FFTBaseAverages.pop_front();
    }
    
    FFTSumP = FFTSumC;
    FFTSumP = FFTBaseSum;

}

Surface32f FFTController::getFFTTexture(int MESH_SIZE){
    int n = 0;
    
    // TODO: Test with more than 2 texture attachments
    
    // FFT 2D texture array
    fftTexture = Surface32f( MESH_SIZE, MESH_SIZE, true);
    Surface32f::Iter pixelIter = fftTexture.getIter();
    while( pixelIter.line() ) {
        while( pixelIter.pixel() ) {
            /* Initial particle positions are passed in as R,G,B
             float values. Alpha is used as particle invMass. */
            fftTexture.setPixel(pixelIter.getPos(),
                                 ColorAf(fftBuffer[n] / FREQBUCKETS * 255.0,
                                         0.0,
                                         0.0,
                                         0.0 ) );
            n++;
        }
        n = 0;
    }
    return fftTexture;
}

int FFTController::getFFTSum(int startBand, int endBand){
    float fftSum = 0;
//    float * fftBuffer = mFftDataRef.get();
    for( int i = startBand; i < ( endBand ); i++ ) {
        fftSum += fftBuffer[i];
	}
    return fftSum;
}

int FFTController::getFFTMax(int startBand, int endBand){
    float fftMax = 0;
//    float * fftBuffer = mFftDataRef.get();
    for( int i = startBand; i < ( endBand ); i++ ) {
        fftMax = fmax(fftMax, fftBuffer[i]);
	}
    return fftMax;
}

int FFTController::ListAverage(list<float> mList){
    float a = 0;
    for( list<float>::iterator mFFT = mList.begin(); mFFT != mList.end(); ++mFFT)
    {
        a += *mFFT;
    }
    return a/mList.size();
}

//DRAW FFT STRIP
void FFTController::drawFFT(){
    float ht = 100.0f;
    float bottom = cinder::app::getWindowHeight() - 10.0;
	gl::setMatricesWindow( cinder::app::getWindowSize() );
    
	for( int i = 0; i < ( FREQBUCKETS ); i++ ) {
		float barY = fftBuffer[i] / FREQBUCKETS * ht;
		glBegin( GL_QUADS );
        glColor3f( 255.0f, 255.0f, 0.0f );
        glVertex2f( i * 3, bottom );
        glVertex2f( i * 3 + 1, bottom );
        
        glColor3f( 0.0f, 255.0f, 0.0f );
        glVertex2f( i * 3 + 1, bottom - barY );
        glVertex2f( i * 3, bottom - barY );
		glEnd();
        glColor3f(1,1,1);
	}
}

void FFTController::drawFFTTexture(){
    gl::enable(GL_TEXTURE_2D);
    gl::setMatricesWindow(cinder::app::getWindowSize());
    Rectf preview1 ( 220.0f, 20.0f, 300.0f, 100.0f);
//    gl::draw(, mPPFbo.mFbos[0].getBounds(), preview1);
//    gl::draw(fftTexture);
    gl::disable(GL_TEXTURE_2D);
}


