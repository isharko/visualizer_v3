#include "cinder/app/AppNative.h"
#include "Resources.h"

#include "cinder/gl/gl.h"
#include "cinder/gl/GlslProg.h"
#include "cinder/gl/Fbo.h"
#include "cinder/gl/Vbo.h"
#include "cinder/gl/Fbo.h"

#include "cinder/ImageIo.h"
#include "cinder/Rand.h"
#include "cinder/Utilities.h"
#include "cinder/Camera.h"
#include "cinder/params/Params.h"
#include "PingPongFbo.h"
#include "SpringCam.h"
#include "Controller.h"


using namespace ci;
using namespace ci::app;
using namespace std;

class Visualizer : public AppNative {
  public:
	void setup();
	virtual void	mouseDown( MouseEvent event );
	virtual void	mouseUp( MouseEvent event );
	virtual void	mouseMove( MouseEvent event );
	virtual void	mouseDrag( MouseEvent event );
	void            update();
	void            draw();
    void            prepareSettings(ci::app::AppBasic::Settings * settings);

    //VARS
    int             MESH_SIZE       = 80;
    
    //CONTROLLER
    Controller      mController;

    
    //CAMERA
//    CameraPersp				mCam;
	Quatf					mSceneRotation;
    Vec3f                   mEye, mCenter, mUp;
    void                    setupCamera();
    float                   mCameraDistance;
    Vec3f                   bRight, bUp;
    
    // CAMERA
	SpringCam               mSpringCam;
    
    //MOUSE
	Vec2f                   mMousePos, mMouseDownPos, mMouseOffset;    
    bool                    mMousePressed;
    
    //PARAMS
	params::InterfaceGlRef	mParams;
    float                   mFps;
    bool                    mShowFbo;
    bool                    mShowFFT;

    
    //SHADER
    ci::gl::GlslProg mShader;
    ci::gl::GlslProg mPositionsShader;
    void enablePointSprites();
    void disablePointSprites();
    
    //VBO
    void initMesh();
    std::vector<uint32_t>   mVboIndices;
    std::vector<Vec2f>      mVboTexCoords;
    std::vector<Vec3f>      mVboVertices;
    ci::gl::VboMesh::Layout mVboLayout;
    ci::gl::VboMesh         mVboMesh;
    int                     mParticleSize   = 2;
    
    //FBO
    void drawPositionsToPPFbo();
    PingPongFbo             mPPFbo;
    void                    setupPingPongFbo();
    
    
    //TEXTURE
    gl::Texture             mTexturePhoto;
    gl::Texture             mParticleTex;
    gl::Texture             mPositionsTex;
    
    //VARS
    

    float                   STEP            = 2.0f;
    float                   phase           = 0.0f;
    float                   speed           = 0.02f;
    float                   frame           = 0.0f;
    
};

void Visualizer::setup()
{
    //PARAMETERS AND CAMERA
    mParams = params::InterfaceGl::create( getWindow(), "App parameters", toPixels( Vec2i( 200, 400 ) ) );
    mParams->addParam("Scene Rotation",     &mSceneRotation);
    mParams->addParam("Eye Distance",       &mCameraDistance, "min=-3000.0 max=3000.0 step=2.0 keyIncr=[ keyDecr=]");
    mParams->addParam("Show FBO",           &mShowFbo,        "key=d");
    mParams->addParam("Show FFT",           &mShowFFT,        "key=f");
    
    setupCamera();
    mParams->addParam("FPS", &mFps);
    
    mShowFFT        = true;
    mShowFbo        = true;
    
    //LOAD SHADER
    int32_t maxGeomOutputVertices;
    glGetIntegerv(GL_MAX_GEOMETRY_OUTPUT_VERTICES_EXT, &maxGeomOutputVertices);
    try {
        mPositionsShader = gl::GlslProg( loadResource(POSITIONS_VERT), loadResource(POSITIONS_FRAG) );
        mShader          = gl::GlslProg( loadResource(MESH_VERT), loadResource(MESH_FRAG) );
    }
    catch( ci::gl::GlslProgCompileExc &exc ) {
        std::cout << "Shader compile error: " << endl;
        std::cout << exc.what();
    }
    catch( ... ) {
        std::cout << "Unable to load shader" << endl;
    }
    
    //LOAD TEXTURE
    mTexturePhoto = gl::Texture(loadImage(loadResource(PHOTO_TEXTURE)));
    mParticleTex = gl::Texture(loadImage(loadResource(PARTICLE_TEX)));
    mPositionsTex = gl::Texture(loadImage(loadResource(POSITIONS_TEX)));
    
    //SETUP FBO
    setupPingPongFbo();
    
    
    //SETUP VBO LAYOUT
    mVboLayout.setStaticIndices();
    mVboLayout.setStaticPositions();
    mVboLayout.setStaticTexCoords2d();

    //INIT MESH
    initMesh();
    
    //INIT CONTROLLER
    mController = Controller(MESH_SIZE);
    
    //MOUSE
    mMousePos			= Vec2f::zero();
	mMouseDownPos		= Vec2f::zero();
	mMouseOffset		= Vec2f::zero();
    mMousePressed       = false;
}

//INIT MESH ----------------------------------------------------------------------

void Visualizer::initMesh()
{
    //SETUP VBO QUAD - a simple quad, for billboarding
    int i = 0;
    while(i < 4){
        mVboIndices.push_back(i);
        i++;
    }
    
    mVboTexCoords.push_back(Vec2f(0, 1));
    mVboTexCoords.push_back(Vec2f(1, 1));
    mVboTexCoords.push_back(Vec2f(1, 0));
    mVboTexCoords.push_back(Vec2f(0, 0));
    
    mVboVertices.push_back(Vec3f(-mParticleSize,mParticleSize,1));
    mVboVertices.push_back(Vec3f(mParticleSize,mParticleSize,1));
    mVboVertices.push_back(Vec3f(mParticleSize,-mParticleSize,1));
    mVboVertices.push_back(Vec3f(-mParticleSize,-mParticleSize,1));
        
    //SETUP VBO BUFFER
    if(mVboMesh)
        mVboMesh.reset();
    mVboMesh = gl::VboMesh(mVboIndices.size(), mVboIndices.size(), mVboLayout, GL_QUADS);
    mVboMesh.bufferIndices(mVboIndices);
    mVboMesh.bufferPositions(mVboVertices);
    mVboMesh.bufferTexCoords2d(0, mVboTexCoords);
    mVboMesh.unbindBuffers();
    
    //Clean up
    mVboIndices.clear();
    mVboTexCoords.clear();
    mVboVertices.clear();
}

//DRAW POSITIONS TO PING PONG FBO BUFFER----------------------------------------------------------------------

void Visualizer::drawPositionsToPPFbo(){
    gl::setMatricesWindow(mPPFbo.getSize(), false);
    gl::setViewport(mPPFbo.getBounds());
    mPPFbo.updateBind();
    mPositionsShader.bind();
    mPositionsShader.uniform("phase", phase);
    mPositionsShader.uniform("parameter", Rand::randFloat(0, 3000.0));
//    mPositionsShader.uniform("fftTexture", mController.mFFTController.getFFTTexture(MESH_SIZE));    
    gl::drawSolidRect(mPPFbo.getBounds());
    mPositionsShader.unbind();
    mPPFbo.updateUnbind();
    mPPFbo.swap();
}


//SETUP PING PONG FBO ----------------------------------------------------------------------
void Visualizer::setupPingPongFbo()
{
    float n = 0;
    float nRow = 0;
    
    // TODO: Test with more than 2 texture attachments
    Surface32f surfaces[2];
    // Position 2D texture array
    surfaces[0] = Surface32f( MESH_SIZE, MESH_SIZE, true);
    Surface32f::Iter pixelIter = surfaces[0].getIter();
    while( pixelIter.line() ) {
        while( pixelIter.pixel() ) {
            /* Initial particle positions are passed in as R,G,B
             float values. Alpha is used as particle invMass. */
            surfaces[0].setPixel(pixelIter.getPos(),
                                 ColorAf(n,
                                         nRow,
                                         0.0,
                                         Rand::randFloat(0.0, 5.0) ) );
            n++;
        }
        nRow ++;
        n = 0;
    }
    
    //Velocity 2D texture array
    surfaces[1] = Surface32f( MESH_SIZE, MESH_SIZE, true);
    pixelIter = surfaces[1].getIter();
    while( pixelIter.line() ) {
        while( pixelIter.pixel() ) {
            /* Initial particle velocities are
             passed in as R,G,B float values. */
            surfaces[1].setPixel( pixelIter.getPos(), ColorAf( 0.0f, 0.0f, 0.0f, 1.0f ) );
        }
    }
    mPPFbo = PingPongFbo(surfaces);
}


void Visualizer::mouseDown( MouseEvent event )
{
	mMouseDownPos = event.getPos();
	mMousePressed = true;
	mMouseOffset = Vec2f::zero();
}

void Visualizer::mouseUp( MouseEvent event )
{
	mMousePressed = false;
	mMouseOffset = Vec2f::zero();
}

void Visualizer::mouseMove( MouseEvent event )
{
	mMousePos = event.getPos();
}

void Visualizer::mouseDrag( MouseEvent event )
{
	mouseMove( event );
	mMouseOffset = ( mMousePos - mMouseDownPos );
}

//UPDATE ----------------------------------------------------------------------

void Visualizer::update()
{
    phase+= speed;
//    std::cout <<  mSpringCam.getCam().getOrientation() << std::endl;
    
    // Update camera persp
    mSpringCam.mCamDist = mCameraDistance;
    if( mMousePressed ){
		mSpringCam.dragCam( ( mMouseOffset ) * 0.01f, ( mMouseOffset ).length() * 0.01 );
	}
	mSpringCam.update( 0.4f );//mTimeAdjusted );
    
    // Update Controller
    mController.update(0.1f);
    
    //Draw Positions to Ping Pong FBO
    drawPositionsToPPFbo();

}

//DRAW ----------------------------------------------------------------------

void Visualizer::draw()
{
	// clear out the window with black
    gl::setViewport( getWindowBounds() );
	gl::clear( Color::black() );
    
    ////////////////////////////////////////////////////////////////////////////////////
    
    gl::enable( GL_TEXTURE_2D );
    gl::enableAlphaBlending();
    

    mPPFbo.bindTexture(0);
    mTexturePhoto.bind(2);
    mParticleTex.bind(3);
    
    gl::setViewport (getWindowBounds());
    gl::setMatrices(mSpringCam.getCam());
    
    {
        mShader.bind();
        mShader.uniform("positions", 0);
        mShader.uniform("velocities", 1);
        mShader.uniform("photo", 2);
        mShader.uniform("photoSize", Vec2f(mTexturePhoto.getSize()));
        mShader.uniform("particleTex", 3);
        mShader.uniform("farClip", mSpringCam.mCamDist-1400.0f);
        mShader.uniform("right", bRight);
        mShader.uniform("up", bUp);
        
        mVboMesh.enableClientStates();
        mVboMesh.bindAllData();
        GLsizei count = (GLsizei)( mTexturePhoto.getWidth() * mTexturePhoto.getHeight() );
        glDrawElementsInstancedARB( mVboMesh.getPrimitiveType(), mVboMesh.getNumIndices(), GL_UNSIGNED_INT, (GLvoid*)0, count );
        gl::VboMesh::unbindBuffers();
        mVboMesh.disableClientStates();
        
        mShader.unbind();
        mParticleTex.unbind();
        mTexturePhoto.unbind();
        mPPFbo.unbindTexture();
    }
        
    gl::enableAlphaBlending();
    
    
    ////////////////////////////////////////////////////////////////////////////////////
    
    //Draw FBO
    if (mShowFbo){

        gl::enable(GL_TEXTURE_2D);
        gl::setMatricesWindow(getWindowSize());
        Rectf preview1 ( 220.0f, 20.0f, 300.0f, 100.0f);
        gl::draw(mPPFbo.mFbos[0].getTexture(), mPPFbo.mFbos[0].getBounds(), preview1);

        Rectf preview2 ( 220.0f, 120.0f, 300.0f, 200.0f);
        gl::draw(mPPFbo.mFbos[1].getTexture(), mPPFbo.mFbos[0].getBounds(), preview2);        
        gl::disable(GL_TEXTURE_2D);
    }
    
    //Controller - Draw things
    if (mShowFFT){
        mController.drawFFT();
    }
    
    mFps = getAverageFps();
    mParams->draw();
}

void Visualizer::setupCamera(){
//    mCam.setPerspective(60.0, getWindowAspectRatio(), 5.0f, 1000.0f);
//    mEye = Vec3f(0.0, 0.0f, 0.0f);
//    mCenter = Vec3f(0.0f, 0.0f, 0.0f);
//    mUp = Vec3f::yAxis();
    mCameraDistance                 = -500.0f;
    mSpringCam                      = SpringCam( mCameraDistance, getWindowAspectRatio() );
    mSpringCam.mEyeNode.mRestPos    = Vec3f( 0.0f, 80.0f, -466.0f );
}

// Prepare window settings
void Visualizer::prepareSettings(ci::app::AppBasic::Settings * settings)
{
    // Do settings
	settings->setTitle("GeomShaderApp");
	settings->setWindowSize(1280, 720);
	settings->setFrameRate(30.0f);
	settings->setFullScreen(false);
    
}


CINDER_APP_NATIVE( Visualizer, RendererGl )
