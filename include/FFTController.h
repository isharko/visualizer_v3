
#pragma once

#include "cinder/Vector.h"
#include <list>
#include "cinder/audio/Input.h"
#if defined( CINDER_COCOA_TOUCH )
#include "cinder/app/AppCocoaTouch.h"
typedef ci::app::AppCocoaTouch AppBase;
#else
#include "cinder/app/AppBasic.h"
#include "cinder/audio/FftProcessor.h"
typedef ci::app::AppBasic AppBase;
#endif

#include "Resources.h"

using namespace ci;
using std::list;


class FFTController{
 public:
	FFTController();
    void update();
    int getFFTSum(int startBand, int endBand);
    int getFFTMax(int startBand, int endBand);
    int ListAverage(list<float>mList);
    Surface32f getFFTTexture(int TEXTURE_SIZE);
    void drawFFTTexture();
    void drawFFT();
    
    float   FFTDifference;
    float   FFTSumC;
    float   FFTSumP;
    float   FFTBaseDifference;
    float   FFTBaseAverage;
    float   FFTBaseSum;
    float   FFTBaseP;
    float   FFTBaseMax;
    float   FFTAverage;
    float   FREQBUCKETS;
    float   BASEBANDS;
    float   *fftBuffer;
    
    
    bool        BASEKICK    = false;
    bool        BIGBASEBOOM = false;
    int         cNotBaseKick = 0;
    int         cNotBaseKickMax = 3;
    Surface32f  fftTexture;
    
    float   prevBoom = 0;
    
    bool    quiet = false;
    int     k = 0;

    
    list<float>     FFTBaseAverages;

    
    audio::Input mInput;
    
	std::shared_ptr<float> mFftDataRef;
	audio::PcmBuffer32fRef mPcmBuffer;
};
