//
//  Controller.h
//  Visualizer_v3
//
//  Created by Ivan Sharko on 2013-05-28.
//
//

#ifndef __Visualizer_v3__Controller__
#define __Visualizer_v3__Controller__

#include <iostream>

#include "FFTController.h"

class Controller {
    public:
    Controller();
    Controller(int &MESH_SIZE);
    
    void update( float dt );
    void updateFFT();
    void drawFFT();
    void drawFFTTexture();
    
    Surface32f mFftTexture;
    
    int mMeshSize;
    
    FFTController mFFTController;
};

#endif /* defined(__Visualizer_v3__Controller__) */
