#pragma once
#include "cinder/CinderResources.h"

//#define RES_MY_RES			CINDER_RESOURCE( ../resources/, image_name.png, 128, IMAGE )

#define POSITIONS_FRAG          CINDER_RESOURCE( ../resources/, Positions.frag, 128, GLSL )
#define POSITIONS_VERT          CINDER_RESOURCE( ../resources/, passThrough.vert, 129, GLSL )

#define MESH_FRAG               CINDER_RESOURCE( ../resources/, Particles.frag, 130, GLSL )
#define MESH_VERT               CINDER_RESOURCE( ../resources/, Particles.vert, 131, GLSL )


#define PARTICLE_TEX            CINDER_RESOURCE( ../resources/, corona.png, 132, IMAGE )
#define PHOTO_TEXTURE           CINDER_RESOURCE( ../resources/, tulisa_s.jpg, 133, IMAGE )
#define POSITIONS_TEX           CINDER_RESOURCE( ../resources/, positions.jpg, 135, IMAGE )



