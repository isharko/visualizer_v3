//// Inputs
//uniform float amp;
//uniform float phase;
//uniform float rotation;
//uniform float scale;
//uniform float size;
//uniform float speed;
//uniform float width;
//
//// Kernel
//void main(void)
//{
//
//	// Position vertex from time
//	vec4 vertex = gl_Vertex;
//	float wave = (sin((phase * speed) + vertex.x * width)) * (amp * scale);
//	gl_Position = gl_ModelViewProjectionMatrix * vec4(scale * vertex.x, scale * vertex.y + wave, scale * vertex.z - wave, 1.0);
//
//}

uniform sampler2D imageMap;
varying vec4 texCoord;
varying vec4 color;

const float SIZE  = 2000.0;

void main()
{
    //calculate distance of the vertex to the camera 
    vec3 vertex = vec3(gl_ModelViewProjectionMatrix * gl_Vertex);
    float distance = length(vertex);
    
	texCoord = gl_MultiTexCoord0;
    gl_PointSize = 1.0 / distance * SIZE;
//    color = texture2D( imageMap, gl_MultiTexCoord0.xy );
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}