#version 120
// Kernel

uniform float speed;
uniform float width;
uniform float mColor;
varying vec4 color;

uniform sampler2D particleTex;


void main(void)
{

	// White
//	gl_FragColor = vec4(width, speed, 1.0, 1.0);

    //FROM ORIGINAL GPU RENDERER
//    gl_FragColor = vec4(abs(color));
    
    
    
    //ATTEMPT FROM A STAR FILE
    vec4 star = texture2D(particleTex, gl_PointCoord);
    gl_FragColor = clamp(star, 0.0, 1.0);

    
}
