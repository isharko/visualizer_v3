//input
uniform float parameter;
uniform float phase;
uniform sampler2D fftTexture;

//vars
varying vec4 texCoord;

void main(void){
    gl_FragColor = vec4(texCoord.x, texCoord.y, sin(phase + texCoord.x + texCoord.y)/2.0, 1.0);
//      gl_FragColor = vec4(texCoord.x, texCoord.y, texCoord.z + fftTexture.z, 1.0);


//    gl_FragColor = vec4(texCoord.x, texCoord.y, texCoord.z, 1.0);

}


