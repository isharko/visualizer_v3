#version 120
//input
uniform sampler2D particleTex;

//vars
varying vec4 imageMapColor;

void main(void){
    vec4 color    = texture2D( particleTex, gl_PointCoord ) * imageMapColor;
    gl_FragColor = color;
}