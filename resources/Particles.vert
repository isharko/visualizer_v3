//// Inputs
//uniform float amp;
//uniform float rotation;
//uniform float scale;
//uniform float size;

//
//// Kernel
//void main(void)
//{
//
//	// Position vertex from time
//	vec4 vertex = gl_Vertex;
//	float wave = (sin((phase * speed) + vertex.x * width)) * (amp * scale);
//	gl_Position = gl_ModelViewProjectionMatrix * vec4(scale * vertex.x, scale * vertex.y + wave, scale * vertex.z - wave, 1.0);
//
//}


#version 120
#extension GL_EXT_gpu_shader4 : enable
#extension GL_ARB_draw_instanced : enable

//Inputs
uniform float       farClip;
uniform sampler2D   photo;
uniform vec2        photoSize;
uniform sampler2D   positions;
uniform vec3        up;
uniform vec3        right;


varying vec4 color;
varying vec2 texCoord;

void main( void )
{
    texCoord = gl_MultiTexCoord0.st;
    
    vec3 vertex = vec3(gl_ModelViewProjectionMatrix * gl_Vertex);
    float distance = length(vertex);
    
    float u = float( mod( float( gl_InstanceID ), photoSize.x ) );
    float v = float( floor( gl_InstanceID / photoSize.x ) );
    vec2 uv = vec2( u, v ) / photoSize;
    color = texture2D( photo, uv );

    float scale =0.9;
    
    vec3 pos = texture2D( positions, uv ).xyz;
    pos *= vec3( 2.0 );
    pos -= vec3( 1.0 );
    pos *= farClip;
    pos /=10.0;
//    pos = vec3(0,0,0);
    
    vec3 localPos = gl_Vertex.xyz;
    
    //Adjust scale of the particle by multiplying localPos & a variable
//    pos += localPos * color.x;
  
    pos += localPos * 1.0;

    gl_Position = gl_ModelViewProjectionMatrix * vec4( pos, 1.0 );
}