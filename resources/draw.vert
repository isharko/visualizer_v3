//// Inputs
//uniform float amp;
//uniform float rotation;
//uniform float scale;
//uniform float size;

//
//// Kernel
//void main(void)
//{
//
//	// Position vertex from time
//	vec4 vertex = gl_Vertex;
//	float wave = (sin((phase * speed) + vertex.x * width)) * (amp * scale);
//	gl_Position = gl_ModelViewProjectionMatrix * vec4(scale * vertex.x, scale * vertex.y + wave, scale * vertex.z - wave, 1.0);
//
//}


#version 120
//Inputs
uniform sampler2D photo;
uniform sampler2D particleTex;

//Vards
varying vec4 imageMapColor;
const float SIZE  = 500.0;

void main(){
    imageMapColor   = texture2D( particleTex, gl_MultiTexCoord0.st );
    
    //calculate distance of the vertex to the camera
    vec3 vertex     = vec3(gl_ModelViewProjectionMatrix * gl_Vertex);
    float distance  = length(vertex);
    
    gl_PointSize    = 1.0 / distance * SIZE;
    gl_Position     = gl_ModelViewProjectionMatrix * gl_Vertex;
}