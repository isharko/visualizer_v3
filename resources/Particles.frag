#version 120

uniform sampler2D particleTex;

varying vec4 color;
varying vec2 texCoord;

void main( void )
{
    gl_FragColor = texture2D( particleTex, texCoord ) * color;
}